# sparkfun

These are Python3 implementations for certain SparkFun sensors. Some are coarse prototypes just to get the values quick out. Some are more refined.

These modules are provided 'as is', without any promise for support.

These modules are mainly intended to be used with Raspberry Pi.

These codes are provided mainly for academic purposes and learning. They are research prototype codes and the level of commentary and refinement varies.

Some of these may follow parts of Arduino libraries of SparkFun, but none should be a direct port.

Depedencies - Python modules:
* SMBus2
* serial



License:

Copyright 2019 Teemu Laukkarinen

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.