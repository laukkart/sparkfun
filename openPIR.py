import RPi.GPIO as GPIO
import time


class PIR:
    '''PIR is an RPi class for SparkFun OpenPIR -module (SEN-13968)
    
    Dependencies: requires RPi.GPIO python module, and the PIR module should be in dual mode

    Use:
    At initialization provide the channel that the OpenPIR module is connected to
    You may poll, wait, set/remove callbacks, and use the PIR as counter
    DO NOT set your own callbacks and use the PIR at the same time as counter. This is not (yet) supported
    The callback should take one parameter: channel
    The counter is initialized to 0 at the start of the counter

    There is not yet support for falling edge
    '''
    usleep = lambda z,x: time.sleep(x/1000000.0)
    debug = lambda z,msg: print(__class__.__name__ + ": " + msg)
    MODE_NORMAL = 1
    MODE_ISR = 2
    MODE_COUNTER = 3

    # channel 26 is GPIO07 or SPI_CE1_N or D7 in SparkFun Qwiiic board
    def __init__(self, channel=26):
        self._channel = channel
        self._mode = self.MODE_NORMAL
        self._counter = 0
        
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self._channel, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    
    def poll(self):
        return (GPIO.input(self._channel) == 1)

    def wait(self, poll=100):
        if GPIO.input(self._channel) == 0:
            GPIO.wait_for_edge(self._channel, GPIO.RISING)
            
    def set_callback(self, callback):
        self._mode = self.MODE_ISR
        GPIO.add_event_detect(self._channel, GPIO.RISING, callback=callback)

    def remove_callback(self):
        self._mode = self.MODE_NORMAL
        GPIO.remove_event_detect(self._channel)

    def counter(self, channel):
        self._counter += 1
        
    def start_counter(self):
        if self._mode == self.MODE_NORMAL:
            self._counter = 0
            self._mode = self.MODE_COUNTER
            self.set_callback(self.counter)
        else:
            debug("ERROR! Cannot start counter if mode is not normal")

    def get_counter(self):
        return self._counter

    def stop_counter(self):
        self.remove_callback()
        return self._counter
        
            
    def __del__(self):
        GPIO.cleanup()


    
