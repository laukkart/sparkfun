import smbus2
import time

class GSXZ01:

    usleep = lambda z,x: time.sleep(x/1000000.0)
    debug = lambda z,msg: print(__class__.__name__ + " at I2C " + str(z._i2caddr) + ": " + msg)

    REG_STATUS = 0x00
    REG_DATA_READY = 0x01
    REG_DATA_READY_CONFIG = 0x02

    REG_GESTURE = 0x04

    REG_GESTURE_SPEED = 0x05

    REG_CONFIDENCE = 0x06

    REG_ID = 0xFF

    GESTURES = {0x00:"UNKOWN", 0x01:"RIGHT SWIPE", 0x02:"LEFT SWIPE", 0x03:"UP SWIPE" }
    
    def __init__(self, bus, i2caddr, debug=False):
    
        self._i2caddr = i2caddr
    
        if not debug:
            # turn off debugging
            # there's no need for z to eat out the self here for some reason
            self.debug = lambda msg: None

        # read ID
        id = bus.read_byte(self._i2caddr, self.REG_ID)
        if id == 0x01:
            self.debug("XYZ gesture sensor indetified")
        else:
            ## TODO: throw exception
            pass

    def gesture_blocking(self, bus):
        retry = True
        while retry:
            gesture = self.gesture_polling(bus)
            if gesture == None:
                usleep(100000)
            else:
                retry = False
        return gesture

    def gesture_polling(self, bus):
        if bus.read_byte(self._i2caddr, self.REG_STATUS) & 0b00000100:
            gesture = bus.read_byte(self._i2caddr, self.REG_GESTURE)
            self.debug("Read gesture " + str(gesture))
            if gesture < 0x04:
                return self.GESTURES[gesture]
            elif gesture >= 0x04:
                return "HOVER"
            else:
                return None
