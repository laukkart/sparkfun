import smbus2
import time
import sys


class APDS9301:
    '''
    A class to read APDS9301 sensor over I2C

    Depedencies:
    smbus2
    
    Usage notices:
    - Class will pass through any smbus2 exceptions
    - smbus2 is not stored internally so that same instance may be shared, and if it fails, it must re-initialized by the user of this class
    - i2c addr is stored internally so that multiple instances of this class can be created with different addr
    - internal debugging can be made visible via constructor's debug parameter
    '''

    # the z here eats out the implicit "self", when this is called within methods
    usleep = lambda z,x: time.sleep(x/1000000.0)
    debug = lambda z,msg: print(__class__.__name__ + " at I2C " + str(z._i2caddr) + ": " + msg)
    
    CMD_BYTE = 0b11000000
    CMD_WORD = 0b11100000
    ID = 0x0A
    CTRL = 0x00
    TIMING = 0x01
    DATA0LOW = 0x0C
    DATA0HIGH = 0x0D
    DATA1LOW = 0x0E
    DATA1HIGH = 0x0F
    
    def __init__(self, bus, i2caddr, debug=False):
        '''
        Constructor for APDS9301
        
        Parameters
        bus: the smbus instance to a i2c bus
        i2caddr: the i2c address of the APDS9301
        debug: defaults to False, if turned on with True, prints debug information to stdout that may help resolve issues
        '''
        self._i2caddr = i2caddr
        
        if not debug:
            # turn off debugging
            # there's no need for z to eat out the self here for some reason
            self.debug = lambda msg: None
        
        self.debug("Initializing")
        
        # turns sensor on
        bus.write_byte_data(self._i2caddr, (self.CMD_BYTE | self.CTRL), 0x03)

        # identify
        id = bus.read_byte_data(self._i2caddr, (self.CMD_BYTE | self.ID))
        if not( id & 0xF0 == 0b01010000):
            self.debug("Did not identify")
            return

        # bit 4 is gain, 0 for low, 1 for high
        # bits 1:0 are integration time, 00 = 13.7ms, 01=101ms, 10=402ms, 11=manual
        bus.write_byte_data(self._i2caddr, (self.CMD_BYTE | self.TIMING), 0x12)
        
        self.debug("Initialization successful")

    def read(self, bus):
        '''
        Read method for APDS9301

        Return:
        The sensor reading as LUX (float)
        '''
        adc0 = bus.read_word_data(self._i2caddr, (self.CMD_WORD | self.DATA0LOW))
        adc1 = bus.read_word_data(self._i2caddr, (self.CMD_WORD | self.DATA1LOW))
        self.debug("Read adc0=" + str(adc0) + " adc1=" + str(adc1) + " LUX=" + str(self.to_lux(adc0, adc1)))
        return self.to_lux(adc0, adc1)


    def to_lux(self, adc0, adc1):
        if adc0 == 0:
            adc0 = 1
        div = adc1 / adc0
        if div <= 0.5:
            return ((0.0304 * adc0) - (0.062 * adc0 * pow(div,1.4)))
        if div > 0.5:
            if div <= 0.61:
                return ((0.0224 * adc0) - (0.031 * adc1))
            elif div <= 0.80:
                return ((0.0128 * adc0) - (0.0153 * adc1))
            elif div <= 1.30:
                return ((0.00146 * adc0) - (0.00112 * adc1))
            else:
                return 0.0
        return 0.0
