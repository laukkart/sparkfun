import smbus2
import time




class RFD77402:

    usleep = lambda z,x: time.sleep(x/1000000.0)
    debug = lambda z,msg: print(__class__.__name__ + " at I2C " + str(z._i2caddr) + ": " + msg)

    # 8 bit R/W
    REG_ICSR = 0x00
    REG_ER = 0x02
    REG_CMD = 0x04

    # 16bit read only
    REG_STATUS = 0x06
    REG_RESULT = 0x08
    REG_CONFIDENCE = 0x0A

    REG_CHIP_ID = 0x28

    def __init__(self, bus, i2caddr=0x4C, debug=False):
        self._i2caddr = i2caddr

        if not debug:
            # turn off debugging
            # there's no need for z to eat out the self here for some reason
            self.debug = lambda msg: None

        self.debug("Initializing")
        
        # NOTE: this follows directly the sensor specification flow chart, hence the given hexadecimals are used directly
        
        # set standby to ensure that if the device is already running, it will return to stand by
        bus.write_byte_data(self._i2caddr, 0x04, 0x90)
        
        retry = True
        while retry:
            ret = bus.read_byte_data(self._i2caddr, 0x06)
            self.debug("Read from 0x06 " + str(ret))
            if (ret & 0x1F) == 0x00:
                retry = False
            else:
                self.usleep(10000)

        self.debug("In standby")
        
        # skip interrupts?
        bus.write_byte_data(self._i2caddr, 0x00, 0x04)

        # initialize i2c to auto increment 8bit mode
        bus.write_byte_data(self._i2caddr, 0x1C,  0x65)

        # this apparently enables patch code mode (needs to be cleared when done) and prevents MCPU from running until let
        bus.write_byte_data(self._i2caddr, 0x15, 0x05)

        self.debug("Turning MCPU off")
        # Turn off MCPU?
        bus.write_byte_data(self._i2caddr, 0x04, 0x91)

        # check turning off
        retry = True
        while retry:
            ret = bus.read_byte_data(self._i2caddr, 0x06)
            self.debug("Read from 0x06 " + str(ret))
            if (ret & 0x1F == 0x10):
                retry = False
            else:
                self.usleep(10000)

        # set initialization, disables patch code and sets MCPU running??
        bus.write_byte_data(self._i2caddr, 0x15, 0x06)

        self.debug("Turning MCPU on")
        # turns MCPU on? 100 10010 (0x01 single meas, 0x10 stndby, 0x11 OFF, 0x12 ON), 7bit ack bit
        bus.write_byte_data(self._i2caddr, 0x04, 0x92)

        # check is it on
        retry = True
        while retry:
            ret = bus.read_i2c_block_data(self._i2caddr, 0x06, 2)
            self.debug("Read from 0x06 " + str(ret))
            if (ret[0] == 0xB8):
                retry = False
            else:
                self.usleep(10000)
                
        self.debug("MCU is on, writing config")
        
        # configure
        bus.write_i2c_block_data(self._i2caddr, 0x0C, [0x00,0xE1])
        bus.write_i2c_block_data(self._i2caddr, 0x0E, [0xFF, 0x10])
        bus.write_i2c_block_data(self._i2caddr, 0x20, [0xD0, 0x07])
        bus.write_i2c_block_data(self._i2caddr, 0x22, [0x08, 0x50])
        bus.write_i2c_block_data(self._i2caddr, 0x24, [0x41, 0xA0])
        bus.write_i2c_block_data(self._i2caddr, 0x26, [0xD4, 0x45])

        # set standby
        bus.write_byte_data(self._i2caddr, 0x04, 0x90)

        retry = True
        while retry:
            ret = bus.read_byte_data(self._i2caddr, 0x06)
            self.debug("Read from 0x06 " + str(ret))
            if (ret & 0x1F) == 0x00:
                retry = False
            else:
                self.usleep(10000)
                
        self.debug("Intialized, in standby, continuing for measurement mode")

        bus.write_byte_data(self._i2caddr, 0x15, 0x05)

        self.debug("Power off")
        bus.write_byte_data(self._i2caddr, 0x04, 0x91)
        retry = True
        while retry:
            ret = bus.read_byte_data(self._i2caddr, 0x06)
            self.debug("Read from 0x06 " + str(ret))
            if (ret & 0x1F == 0x10):
                retry = False
            else:
                self.usleep(10000)
        
        
        bus.write_byte_data(self._i2caddr, 0x15, 0x06)
        
        self.debug("Turning MCPU on again")
        # turns MCPU on? 100 10010 (0x01 single meas, 0x10 stndby, 0x11 OFF, 0x12 ON), 7bit ack bit
        bus.write_byte_data(self._i2caddr, 0x04, 0x92)

        # check is it on
        retry = True
        while retry:
            ret = bus.read_i2c_block_data(self._i2caddr, 0x06, 2)
            self.debug("Read from 0x06 " + str(ret))
            if (ret[0] == 0xB8):
                retry = False
            else:
                self.usleep(10000)
        
        self.debug("Sensor finally intialized, ready for measurements")
        self._initialized = True


    def read(self, bus):        
        bus.write_byte_data(self._i2caddr, 0x04, 0x81)

        retry = True
        while retry:
            ret = (bus.read_byte_data(self._i2caddr, 0x00))
            #self.debug("Read from 0x00 " + str(ret))
            if (ret & 0b00010000) == 0:
                self.usleep(10000)
            else:
                retry = False
                                      
        #while( (bus.read_byte_data(self._i2caddr, 0x00) & 0x10) != 1 ):
        #    self.usleep(10000)
        #    pass

        res = bus.read_i2c_block_data(self._i2caddr, 0x08, 2)
        self.debug("Read form 0x08" + str(res))
        distance = res[0]
        distance >>= 2
        distance = distance | ((0x1F & res[1]) << 6)
        err = (res[1] & 0x60) >> 5

        return (distance, err)

    def detect_obstacle(self, bus):
        (dis, err) = self.read(bus)
        if dis < 2000:
            return dis
        else:
            return None
