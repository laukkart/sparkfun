import smbus2
import time

class CCS811Error(Exception):
    def __init__(self, s):
        self.s = s
    def __str__(self):
        return(self.s)

class CCS811:
    '''
    A class to read CCS811 sensor over I2C

    Depedencies:
    smbus2
    
    Usage notices:
    - Class will pass through any smbus2 exceptions
    - smbus2 is not stored internally so that same instance may be shared, and if it fails, it must re-initialized by the user of this class
    - i2c addr is stored internally so that multiple instances of this class can be created with different addr
    - internal debugging can be made visible via constructor's debug member
    '''
    
    # the z here eats out the implicit "self", when this is called within methods
    usleep = lambda z,x: time.sleep(x/1000000.0)
    debug = lambda z,msg: print(__class__.__name__ + " at I2C " + str(z._i2caddr) + ": " + msg)
    
    REG_STATUS = 0x00  # 1B
    REG_MEAS_MODE = 0x01 # 1B
    REG_ALG_DATA = 0x02  # 2B MSB CO2, following 2B tVOC, max 8B??
    REG_RAW_DATA = 0x03 # 2B
    REG_ENV_DATA = 0x05 # 4B the compensation values for temp and humidity 4B
    REG_BASELINE = 0x11 # 2B the baseline on which CO2 is adjusted
    REG_ERROR_ID = 0xE0 # 1B clears the error as well
    REG_APP_START = 0xF4
    REG_SW_RESET = 0xFF # 4B must have payload of 0x11 0xE5 0x72 0x8A
    SW_RESET_MAGIC_STR = [0x11, 0xE5, 0x72, 0x8A]
    
    def __init__(self, bus, i2caddr, debug=False):
        '''
        Constructor for CCS811
        
        Parameters
        bus: the smbus instance to a i2c bus
        i2caddr: the i2c address of the CCS811
        debug: defaults to False, if turned on with True, prints debug information to stdout that may help resolve issues

        Exceptions:
        Will throw CCS811Error if malfunction or unexpected behavior is detected
        '''
        self._i2caddr = i2caddr
        self._initialized = False

        if not debug:
            # turn off debugging
            # there's no need for z to eat out the self here for some reason
            self.debug = lambda msg: None

        self.debug("Initializing...")
        
        bus.write_i2c_block_data(self._i2caddr, self.REG_SW_RESET, self.SW_RESET_MAGIC_STR)
        self.usleep(10000) # wait over 1ms
        status = bus.read_byte_data(self._i2caddr, self.REG_STATUS)
        self.debug("Bootloader status: " + str(hex(status)))

        if( status & 0x01 ):
            err = bus.read_byte_data(self._i2caddr, self.REG_ERROR_ID)
            self.debug("Bootloader errors: " + str(hex(status)))

        if( status & 0x10):
            self.debug("App valid!")
        else:
            self.debug("ERROR: INVALID APP, aborting")
            raise CSS811Error("CSS811 has invalid app")

        # 0xF4 APP_START, quits the bootloader
        bus.write_byte(self._i2caddr, self.REG_APP_START)

        self.usleep(10000)

        self.debug("Bootloader completed.. checking status")

        # check for errors
        status = bus.read_byte_data(self._i2caddr, self.REG_STATUS)
        if not (status & 0b10000000):
            self.debug("ERROR: Still in bootloader.. abort!")
            #quit()
            raise CSS811Error("CSS811 does not release from bootloader")

        if (status & 0x01):
            # clear errors
            err = bus.read_byte_data(self._i2caddr, self.REG_ERROR_ID)
            self.debug("Errors found: " + str(hex(err)))

        # set the measurement mode 1
        bus.write_byte_data(self._i2caddr, self.REG_MEAS_MODE, 0b00010000)
        confirm = bus.read_byte_data(self._i2caddr, self.REG_MEAS_MODE)
        self.debug("Meas mode 1: " + str(hex(confirm)))

        status = bus.read_byte_data(self._i2caddr, self.REG_STATUS)
        if (status & 0x01):
            # clear errors
            err = bus.read_byte_data(self._i2caddr, self.REG_ERROR_ID)
            self.debug("Errors found: " + str(hex(err)))
        self.debug("Status: " + str(hex(status)))
        self.debug("Initialization ready")
        
        self._initialized = True


    def read_blocking(self, bus):
        '''
        A read method for the CCS811 sensor

        Usage:
        This is a blocking read method. It will block until the sensor indicates it is ready. 
        The method does not have internal timeouts to resolve non-functioning sensor

        Return:
        A dictionary that contains following items:
        "CO2":(int)
        "TVOC":(int)
        '''
        # waits for status register to indicate new measurement
        rdy = False

        self.debug("Read, blocking")
        
        while(not rdy):
            status = bus.read_byte_data(self._i2caddr, self.REG_STATUS)
            self.debug("Status is " + str(hex(status)))
            if( status & 0b00001000):
                rdy = True
                self.debug("Ready for reading")
                
            elif( status & 0b00000001):
                #error, clear it by reading error reg, initialize again?
                err = bus.read_byte_data(self._i2caddr, self.REG_ERROR_ID)
                self.debug("Errors found: " + str(hex(err)))
                raise CSS811Error("CCS811 failure - reinitialization needed")
            else:
                self.usleep(1000000)
                #rdy = True
            # TODO: Should we check for errors

        # read values
        data = bus.read_i2c_block_data(self._i2caddr, self.REG_ALG_DATA, 4)
        
        CO2 = int.from_bytes(data[0:1], byteorder='big', signed=False)
        TVOC = int.from_bytes(data[2:3], byteorder='big', signed=False)
        
        return {"CO2":CO2, "TVOC":TVOC}


    
