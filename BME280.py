import smbus2
import time
import sys


class BME280:
    '''
    A class to read BME280 sensor over I2C

    Depedencies:
    smbus2
    
    Usage notices:
    - Class will pass through any smbus2 exceptions
    - smbus2 is not stored internally so that same instance may be shared, and if it fails, it must re-initialized by the user of this class
    - i2c addr is stored internally so that multiple instances of this class can be created with different addr
    - internal debugging can be made visible via constructor's debug member
    '''

    # the z here eats out the implicit "self", when this is called within methods
    usleep = lambda z,x: time.sleep(x/1000000.0)
    debug = lambda z,msg: print(__class__.__name__ + " at I2C " + str(z._i2caddr) + ": " + msg)
    
    REG_HUM_LSB = 0xFE
    REG_HUM_MSB = 0xFD
    REG_TEMP_XLSB = 0xFC
    REG_TEMP_LSB = 0xFB
    REG_TEMP_MSB = 0xFA
    REG_PRESS_XLSB = 0xF9
    REG_PRESS_LSB = 0xF8
    REG_PRESS_MSB = 0xF7
    REG_RAW_DATA_START = REG_PRESS_MSB
    REG_RAW_DATA_END = REG_HUM_LSB

    REG_CONFIG = 0xF5 # not needed in normal mode?
    REG_CTRL_MEAS = 0xF4 # bit 0,1 = sensor mode (00 sleep, 01/10 forced mode, 11 normal mode); bit 2..4 oversampling of pressure; bits 5..7 oversampling of temperature
    REG_STATUS = 0xF3  # 0 bit = when 0b0, ready; 3 bit when 0b0, data ready (0b1 while converting)
    REG_CTRL_HUM = 0xF2  # 0..2 bits, 000 = not used, 001 = x1, 010 = 2x, 011 =x4, 100 = x8, 101 = x16, requires that CTRL_MEAS is written after
    REG_CALIBRATION_26_START = 0xE1
    REG_CALIBRATION_41_END = 0xF0
    REG_CALIBRATION_0_START = 0x88
    REG_CALIBRATION_25_END = 0xA1
    REG_RESET = 0xE0  # requires write of 0xB6 in order to reset
    REG_ID = 0xD0

    REG_TEMP_CALIBRATION_START = 0x88
    REG_TEMP_CALIBRATION_END = 0x8D

    REG_HUM_CALIBRATION_H1 = 0xA1
    REG_HUM_CALIBRATION_START = 0xE1
    REG_HUM_CALIBRATION_END = 0xE7

    REG_PRES_CALIBRATION_START = 0x8E
    REG_PRES_CALIBRATION_END = 0x9F
    
    
    def __init__(self, bus, i2caddr, debug=False):
        '''
        Constructor for BME280
        
        Parameters
        bus: the smbus instance to a i2c bus
        i2caddr: the i2c address of the BME280
        debug: defaults to False, if turned on with True, prints debug information to stdout that may help resolve issues
        '''
        self._i2caddr = i2caddr
        # these are not class variables, because they are different for all sensors,
        # and someone might want to use several same sensors on a device
        self._temp_compensation = {}
        self._hum_compensation = {}
        self._pres_compensation = {}

        if not debug:
            # turn off debugging
            # there's no need for z to eat out the self here for some reason
            self.debug = lambda msg: None
        
        
        self.debug("Initializing with resetting")
        bus.write_byte_data(self._i2caddr, self.REG_RESET, 0xB6)

        while( bus.read_byte_data(self._i2caddr, self.REG_ID) != 0x60):
            self.usleep(1000)

        self.debug("Identified")

        while( (bus.read_byte_data(self._i2caddr, self.REG_STATUS) & 0b0001) != 0):
            self.usleep(1000)

        self.debug("Ready")

        #TODO: fix these magic numbers?
        bus.write_byte_data(self._i2caddr, self.REG_CTRL_HUM, 0b00000001)
        bus.write_byte_data(self._i2caddr, self.REG_CTRL_MEAS, 0b00100111)

        self.read_temp_compensation(bus)
        self.read_hum_compensation(bus)
        self.read_pres_compensation(bus)
        

    def read_temp_compensation(self, bus):
        rawdata = bus.read_i2c_block_data(self._i2caddr, self.REG_TEMP_CALIBRATION_START, (self.REG_TEMP_CALIBRATION_END - self.REG_TEMP_CALIBRATION_START + 1) )
        # NOTE: these indeed are in little endian order where as the actual raw measurement data are in big endian order
        t1 = int.from_bytes(rawdata[0:2], byteorder='little', signed=False)
        t2 = int.from_bytes(rawdata[2:4], byteorder='little', signed=True)
        t3 = int.from_bytes(rawdata[4:6], byteorder='little', signed=True)
        self._temp_compensation = {"T1":t1, "T2":t2, "T3":t3}


    def read_hum_compensation(self, bus):
        h1 = bus.read_byte_data(self._i2caddr, self.REG_HUM_CALIBRATION_H1)
        rawdata = bus.read_i2c_block_data(self._i2caddr, self.REG_HUM_CALIBRATION_START, (self.REG_HUM_CALIBRATION_END - self.REG_HUM_CALIBRATION_START + 1))

        h2 = int.from_bytes(rawdata[0:2], byteorder='little', signed=True)
        h3 = int(rawdata[2])
        # NOTE: the specification clarifies these
        h4l = rawdata[4] & 0b00001111
        h4h = rawdata[3]
        h4 = int(((h4h << 4) | h4l))
        h5l = rawdata[5]
        h5h = (rawdata[4] & 0b11110000) >> 4
        h5 = int((h5h << 8) | h5l)
        h6 = int(rawdata[-1])
        self._hum_compensation = {"H1":h1, "H2":h2, "H3":h3, "H4":h4, "H5":h5, "H6":h6}



    def read_pres_compensation(self, bus):
        rawdata = bus.read_i2c_block_data(self._i2caddr, self.REG_PRES_CALIBRATION_START, (self.REG_PRES_CALIBRATION_END - self.REG_PRES_CALIBRATION_START + 1) )
        p1 = int.from_bytes(rawdata[0:2], byteorder='little', signed=False)
        p2 = int.from_bytes(rawdata[2:4], byteorder='little', signed=True)
        p3 = int.from_bytes(rawdata[4:6], byteorder='little', signed=True)
        p4 = int.from_bytes(rawdata[6:8], byteorder='little', signed=True)
        p5 = int.from_bytes(rawdata[8:10], byteorder='little', signed=True)
        p6 = int.from_bytes(rawdata[10:12], byteorder='little', signed=True)
        p7 = int.from_bytes(rawdata[12:14], byteorder='little', signed=True)
        p8 = int.from_bytes(rawdata[14:16], byteorder='little', signed=True)
        p9 = int.from_bytes(rawdata[16:18], byteorder='little', signed=True)

        self._pres_compensation = {"P1":p1, "P2":p2, "P3":p3, "P4":p4, "P5":p5, "P6":p6, "P7":p7, "P8":p8, "P9":p9}



    def read_blocking(self, bus):
        '''
        A read method for the BME280 sensor

        Usage:
        This is a blocking read method. It will block until the sensor indicates it is ready. 
        The method does not have internal timeouts to resolve non-functioning sensor

        Return:
        A dictionary that contains following items:
        "temperature":(float) 
        "humidity":(float)
        "pressure": (float)
        '''
        while( bus.read_byte_data(self._i2caddr, self.REG_STATUS) & 0b1001 != 0):
            pass

        rawdata = bus.read_i2c_block_data(self._i2caddr, self.REG_RAW_DATA_START, (self.REG_RAW_DATA_END - self.REG_RAW_DATA_START + 1) )
        rawpress = rawdata[0:3]
        rawtemp = rawdata[3:6]
        rawhum = rawdata[6:8]

        press = int.from_bytes(rawpress, byteorder='big', signed=False)
        press = press >> 4 # this removes the four unused bits in the XLSB

        temp = int.from_bytes(rawtemp, byteorder='big', signed=False)
        temp = temp >> 4 # this removes the four unused bits in the XLSB

        hum = int.from_bytes(rawhum, byteorder='big', signed=False)

        return self.compensate(temp, hum, press)


    #returns
    # temperature as XX.XX Celsius
    # humidity as XX.XXXXX % RH
    # pressure as XX.XXXXX Pa
    def compensate(self, adc_t, adc_h, adc_p):
        # compensation of the raw data, sensor specific
        # temperature:
        var1 = ((((adc_t>>3) - (self._temp_compensation["T1"]<<1)) * self._temp_compensation["T2"]) >> 11)
        var2 = ((((adc_t>>4) - self._temp_compensation["T1"]) * ((adc_t>>4) - self._temp_compensation["T1"]) >> 12) * self._temp_compensation["T3"] >> 14)
        t_fine = var1 + var2
        adc_t = (t_fine * 5 + 128) >> 8
        adc_t = float(adc_t)/100.0


        # humidity:
        var1 = (t_fine - 76800)
        # NOTE: read sensor specification to understand these equations
        var2 = (((((adc_h << 14) - (self._hum_compensation["H4"] << 20) - (self._hum_compensation["H5"] * var1)) + 16384) >> 15) * (((((((var1 * self._hum_compensation["H6"]) >> 10) * (((var1 * self._hum_compensation["H3"]) >> 11) + 32768)) >> 10) + 2097152) * self._hum_compensation["H2"] + 8192) >> 14 ))
        var2 = (var2 - (((((var2 >> 15) * (var2 >> 15)) >> 7) * self._hum_compensation["H1"]) >> 4))
        if var2 < 0:
            var2 = 0
        elif var2 > 419430400:
            var2 = 419430400
        h_fine = var2 >> 12
        adc_h = float(h_fine) / 1024

        #pressure:
        var1 = t_fine - 128000
        var2 = var1 * var1 * self._pres_compensation["P6"]
        var2 = var2 + ((var1 * self._pres_compensation["P5"]) << 17)
        var2 = var2 + (self._pres_compensation["P4"] << 35)
        var1 = ((var1 * var1 * self._pres_compensation["P3"]) >> 8) + ((var1 * self._pres_compensation["P2"]) << 12)
        var1 = (((1 << 47) + var1)) * (self._pres_compensation["P1"]) >> 33
        if var1 != 0:
            p_fine = 1048576 - adc_p
            p_fine = int((((p_fine << 31) - var2) * 3125) / var1)
            var1 = (self._pres_compensation["P9"] * (p_fine >> 13) * (p_fine >> 13)) >> 25
            var2 = (self._pres_compensation["P8"] * p_fine) >> 19
            p_fine = ((p_fine + var1 + var2) >> 8) + (self._pres_compensation["P7"] << 4)
            adc_p = float(p_fine) / 256
        else:
            adc_p = 0.0


        return {"temperature":adc_t, "humidity":adc_h, "pressure":adc_p}
